variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "rgname" {}

provider "azurerm" {
    subscription_id = "060c7910-de04-45a3-9953-ad495ae2be2b"
    client_id       = var.client_id
    client_secret   = var.client_secret
    tenant_id       = var.tenant_id
    version = "=2.0.0"
    features {}
}




resource "azurerm_policy_definition" "policy" {
  name         = "subnet-attached-NSG"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "subnet-attached-NSG"

  metadata = <<METADATA
    {
    "category": "General"
    }

METADATA


  policy_rule = <<POLICY_RULE
    {
    "if": {
        "anyOf": [
          {
            "allOf": [
              {
                "field": "type",
                "equals": "Microsoft.Network/virtualNetworks"
              },
              {
                "field": "Microsoft.Network/virtualNetworks/subnets[*].networkSecurityGroup.id",
                "exists": "false"
              }
            ]
          },
          {
            "allOf": [
              {
                "field": "type",
                "equals": "Microsoft.Network/virtualNetworks/subnets"
              },
              {
                "field": "Microsoft.Network/virtualNetworks/subnets/networkSecurityGroup.id",
                "exists": "false"
              }
            ]
          }
        ]
      },
      "then": {
        "effect": "deny"
      }
    }
    

POLICY_RULE

}

data "azurerm_subscription" "current" {
}

resource "azurerm_policy_assignment" "example" {
  name                 = "subnet-with-NSG"
  scope                = data.azurerm_subscription.current.id
  policy_definition_id = azurerm_policy_definition.policy.id
  description          = "All subnet should be attached with NSG"
  display_name         = "subnet-with-NSG"

}